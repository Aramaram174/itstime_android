package com.example.asus.itstime_android.activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.itstime_android.R;

public class ChangePersonInfoActivity extends AppCompatActivity {
    private EditText person_name;
    private EditText person_surname;
    private Button change_person_save_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_person_info);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_title_layout);
        TextView textView = findViewById(R.id.action_bar_custom_title);
        textView.setText(getResources().getString(R.string.personal_title));

        person_name = findViewById(R.id.person_name);
        person_surname = findViewById(R.id.person_surname);
        change_person_save_btn = findViewById(R.id.change_person_save_btn);
        change_person_save_btn.setOnClickListener(v -> {
            String name = person_name.getText().toString().trim();
            String surname = person_surname.getText().toString().trim();
            if (name.length() == 0){
                Toast.makeText(ChangePersonInfoActivity.this, "Մուտքագրեք", Toast.LENGTH_SHORT).show();
                return;
            }
            if (!isValidName(name)){
                Toast.makeText(ChangePersonInfoActivity.this, "Մուտքագրեք", Toast.LENGTH_SHORT).show();
                return;
            }
            if (surname.length() == 0){
                Toast.makeText(ChangePersonInfoActivity.this, "Մուտքագրեք", Toast.LENGTH_SHORT).show();
                return;
            }
            if (!isValidName(surname)){
                Toast.makeText(ChangePersonInfoActivity.this, "Մուտքագրեք", Toast.LENGTH_SHORT).show();
                return;
            }
        });
    }

    public static boolean isValidName(String name) {
        return name.matches( "[a-zA-Z]*" );
    }
}
