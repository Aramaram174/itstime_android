package com.example.asus.itstime_android.activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.itstime_android.R;

public class ChangeEmailActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText new_email_et;
    private EditText new_email_password;
    private EditText change_email_activation_code;
    private Button send_activation_code_btn;
    private Button save_new_email_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_email);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_title_layout);
        TextView textView = findViewById(R.id.action_bar_custom_title);
        textView.setText(getResources().getString(R.string.change_email_title));

        new_email_et = findViewById(R.id.new_email_et);
        new_email_password = findViewById(R.id.new_email_password);
        change_email_activation_code = findViewById(R.id.change_email_activation_code);
        send_activation_code_btn = findViewById(R.id.send_activation_code_btn);
        save_new_email_btn = findViewById(R.id.save_new_email_btn);
        send_activation_code_btn.setOnClickListener(this);
        save_new_email_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String new_email = new_email_et.getText().toString().trim();
        String password = new_email_password.getText().toString().trim();
        String activation_code = change_email_activation_code.getText().toString().trim();
        switch (v.getId()){
            case R.id.send_activation_code_btn:
                validFields(new_email, password);

//                if ("AAA" != null) {
//                    Toast.makeText(this, "Password!", Toast.LENGTH_LONG).show();
//                    Toast.makeText(this, "Մուտքագրված էլ. փոստը գոյություն ունի!", Toast.LENGTH_LONG).show();
//                }
                break;
            case R.id.save_new_email_btn:
                validFields(new_email, password);

                if (activation_code.length() == 0){
                    Toast.makeText(this, "Մուտքագրեք PIN կոդը", Toast.LENGTH_SHORT).show();
                    return;
                }
                break;
        }
    }

    private void validFields(String email, String password){
        if (email.length() == 0){
            Toast.makeText(this, "Մուտքագրեք նոր էլ.փոստը", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!isValidEmail(email)){
            Toast.makeText(this, "Էլ. փոստի սխալ ձևաչափ", Toast.LENGTH_LONG).show();
            return;
        }
        if (password.length() == 0){
            Toast.makeText(this, "Մուտքագրեք գաղտնաբառը", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    private boolean isValidEmail(String email) {
        return email.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    }
}
