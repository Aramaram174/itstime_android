package com.example.asus.itstime_android.interfaces;

import com.example.asus.itstime_android.models.User;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServicesTutorial {

    @POST("sign-up")
    Call<User> signUp(@Body User user);

    @GET("login")
    Call<User> login(@Body User user);

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://9ced48cd.ngrok.io/api/")
            //.baseUrl("http://192.168.0.108:8080/api/") ipconfig cmd
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
