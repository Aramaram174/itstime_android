package com.example.asus.itstime_android.activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.itstime_android.R;

public class ChangePasswordActivity extends AppCompatActivity {
    private EditText change_password_old_password_et;
    private EditText change_password_new_password_et;
    private EditText change_password_re_new_password_et;
    private Button change_password_save_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_title_layout);
        TextView textView = findViewById(R.id.action_bar_custom_title);
        textView.setText(getResources().getString(R.string.change_password_title));

        change_password_old_password_et = findViewById(R.id.change_password_old_password_et);
        change_password_new_password_et = findViewById(R.id.change_password_new_password_et);
        change_password_re_new_password_et = findViewById(R.id.change_password_re_new_password_et);
        change_password_save_btn = findViewById(R.id.change_password_save_btn);
        change_password_save_btn.setOnClickListener(v -> {
            String old_password = change_password_old_password_et.getText().toString().trim();
            String new_password = change_password_new_password_et.getText().toString().trim();
            String re_new_password = change_password_re_new_password_et.getText().toString().trim();
            if (old_password.length() == 0){
                Toast.makeText(ChangePasswordActivity.this, "Մուտքագրեք", Toast.LENGTH_SHORT).show();
                return;
            }
            if (new_password.length() == 0){
                Toast.makeText(ChangePasswordActivity.this, "Մուտքագրեք", Toast.LENGTH_SHORT).show();
                return;
            }
            if (!isValidPassword(new_password)){
                Toast.makeText(ChangePasswordActivity.this, "Մուտքագրեք", Toast.LENGTH_SHORT).show();
            }
            if (re_new_password.length() == 0){
                Toast.makeText(ChangePasswordActivity.this, "Մուտքագրեք", Toast.LENGTH_SHORT).show();
                return;
            }
            if (re_new_password != new_password){
                Toast.makeText(ChangePasswordActivity.this, "Մուտքագրեք", Toast.LENGTH_SHORT).show();
                return;
            }
        });
    }

    public static boolean isValidPassword(String password) {
        return password.matches( "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$");
    }
}
