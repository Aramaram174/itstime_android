package com.example.asus.itstime_android.activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.asus.itstime_android.R;
public class PersonalActivity extends AppCompatActivity implements View.OnClickListener{
    private RelativeLayout layout_name;
    private RelativeLayout layout_email;
    private RelativeLayout layout_password;
    private Button personal_delete_account_btn;
    private ProgressBar personal_delete_account_pb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_title_layout);
        TextView textView = findViewById(R.id.action_bar_custom_title);
        textView.setText(getResources().getString(R.string.personal_title));

        layout_name = findViewById(R.id.layout_name);
        layout_email = findViewById(R.id.layout_email);
        layout_password = findViewById(R.id.layout_password);
        personal_delete_account_btn = findViewById(R.id.personal_delete_account_btn);
        personal_delete_account_pb = findViewById(R.id.personal_delete_account_pb);

        layout_name.setOnClickListener(this);
        layout_email.setOnClickListener(this);
        layout_password.setOnClickListener(this);
        personal_delete_account_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout_name:
                startActivity(new Intent(PersonalActivity.this, ChangePersonInfoActivity.class));
                break;
            case R.id.layout_email:
                startActivity(new Intent(PersonalActivity.this, ChangeEmailActivity.class));
                break;
            case R.id.layout_password:
                startActivity(new Intent(PersonalActivity.this, ChangePasswordActivity.class));
                break;
            case R.id.personal_delete_account_btn:
                break;
        }
    }
}
