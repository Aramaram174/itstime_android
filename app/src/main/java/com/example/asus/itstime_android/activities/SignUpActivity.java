package com.example.asus.itstime_android.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.itstime_android.R;
import com.example.asus.itstime_android.interfaces.ServicesTutorial;
import com.example.asus.itstime_android.models.User;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.asus.itstime_android.App.Months;

public class SignUpActivity extends BaseActivity {
    private Activity activity = this;
    private EditText signup_name_et;
    private EditText signup_surname_et;
    private EditText signup_email_et;
    private EditText signup_password_et;
    private EditText signup_retype_password_et;
    private Button signup_btn;
    private ProgressBar signup_pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_title_layout);
        TextView textView = findViewById(R.id.action_bar_custom_title);
        textView.setText(getResources().getString(R.string.signup_title));

        signup_email_et = findViewById(R.id.signup_email_et);
        signup_name_et = findViewById(R.id.signup_name_et);
        signup_surname_et = findViewById(R.id.signup_surname_et);
        signup_password_et = findViewById(R.id.signup_password_et);
        signup_retype_password_et = findViewById(R.id.signup_retype_password_et);
        signup_btn = findViewById(R.id.signup_btn);
        signup_btn.setOnClickListener(v -> signup());
        signup_pb = findViewById(R.id.signup_pb);
    }

    private void signup() {
        String email = signup_email_et.getText().toString().trim();
        String name = signup_name_et.getText().toString().trim();
        String surname = signup_surname_et.getText().toString().trim();
        String password = signup_password_et.getText().toString().trim();
        String retypePassword = signup_retype_password_et.getText().toString().trim();

        if (email.length() == 0) {
            Toast.makeText(this, getResources().getString(R.string.signup_enter_email), Toast.LENGTH_SHORT).show();
            return;
        }else if (!isValidEmail(email)) {
            Toast.makeText(this, getResources().getString(R.string.signup_wrong_email), Toast.LENGTH_SHORT).show();
            return;
        }
        if (name.length() == 0){
            Toast.makeText(this, getResources().getString(R.string.signup_enter_name), Toast.LENGTH_SHORT).show();
            return;
        }
        if (!isValidName(name)){
            Toast.makeText(this, getResources().getString(R.string.signup_enter_name_only_latin), Toast.LENGTH_SHORT).show();
            return;
        }
        if (surname.length() == 0){
            Toast.makeText(this, getResources().getString(R.string.signup_enter_surname), Toast.LENGTH_SHORT).show();
            return;
        }
        if (!isValidName(surname)){
            Toast.makeText(this, getResources().getString(R.string.signup_enter_surname_only_latin), Toast.LENGTH_SHORT).show();
            return;
        }
        if (password.length() == 0) {
            Toast.makeText(this, getResources().getString(R.string.signup_generate_password), Toast.LENGTH_SHORT).show();
            return;
        } else if (!isValidPassword(password)) {
            Toast.makeText(this, getResources().getString(R.string.signup_only_latin_min_password), Toast.LENGTH_SHORT).show();
            return;
        }
        if (retypePassword.length() == 0) {
            Toast.makeText(this, getResources().getString(R.string.signup_password_retype), Toast.LENGTH_SHORT).show();
            return;
        } else if (!retypePassword.equals(password)) {
            Toast.makeText(this, getResources().getString(R.string.signup_passwords_does_not_match), Toast.LENGTH_SHORT).show();
            return;
        }
        signup_pb.setVisibility(View.VISIBLE);
        User user = new User();
        user.setEmail(email);
        user.setName(name);
        user.setSurname(surname);
        user.setPassword(password);
        saveData(user);
    }

    private boolean isValidEmail(String email) {
        return email.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    }

    public static boolean isValidName(String name) {
        return name.matches( "[a-zA-Z]*" );
    }

    public static boolean isValidPassword(String password) {
        return password.matches( "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$");
    }

    private void saveData(User user){
        ServicesTutorial servicesTutorial = ServicesTutorial.retrofit.create(ServicesTutorial.class);
        Call<User> call = servicesTutorial.signUp(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                signup_pb.setVisibility(View.GONE);
                if (response.body() != null) {
                    startActivity(new Intent(activity, HomeActivity.class));
                    finish();
                } else {
                    Toast.makeText(activity, getResources().getString(R.string.signup_email_registered), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                signup_pb.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.not_found_internet_connection), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
