package com.example.asus.itstime_android.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.asus.itstime_android.R;
import com.example.asus.itstime_android.activities.NoteActivity;
import com.example.asus.itstime_android.activities.ShoppingActivity;
import com.example.asus.itstime_android.activities.BirthdayActivity;

public class NoteFragment extends Fragment implements View.OnClickListener {
    private LinearLayout layout_note;
    private LinearLayout layout_shopping;
    private LinearLayout layout_birthday;

    public NoteFragment() {}

    public static NoteFragment newInstance() {
        return new NoteFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_note, container, false);
//        layout_note = view.findViewById(R.id.layout_note);
//        layout_shopping = view.findViewById(R.id.layout_shopping);
        layout_birthday = view.findViewById(R.id.layout_birthday);

//        layout_note.setOnClickListener(this);
//        layout_shopping.setOnClickListener(this);
        layout_birthday.setOnClickListener(this);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
//                case R.id.layout_note:
//                    startActivity(new Intent(getContext(), NoteActivity.class));
//                    break;
//                case R.id.layout_shopping:
//                    startActivity(new Intent(getContext(), ShoppingActivity.class));
//                    break;
                case R.id.layout_birthday:
                    startActivity(new Intent(getContext(), BirthdayActivity.class));
                    break;
            }
    }
}
