package com.example.asus.itstime_android;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.asus.itstime_android.models.User;

public class App extends Application {
    private static App instance;
    private SharedPreferences sharedPreferences;
    public static final String PREFERANCES_KEY = "user";

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        sharedPreferences = getSharedPreferences(PREFERANCES_KEY, Context.MODE_PRIVATE);
    }
    public static App i(){
        if(instance == null){
            instance = new App();
        }
        return instance;
    }

    public void saveRemember(boolean isRemember){
        sharedPreferences.edit().putBoolean("remember",isRemember).apply();
    }

    public boolean isRememeber(){
        return sharedPreferences.getBoolean("remember",false);
    }

    public void saveUserData(User user){
        sharedPreferences.edit().putString("id",user.getId()).apply();
        sharedPreferences.edit().putString("email",user.getEmail()).apply();
        sharedPreferences.edit().putString("name",user.getName()).apply();
        sharedPreferences.edit().putString("surname",user.getSurname()).apply();
        sharedPreferences.edit().putString("birthday",user.getBirthday()).apply();
        sharedPreferences.edit().putString("created",user.getCreated()).apply();
    }

    public User getUserData(){
        User user = new User();
        user.setId(sharedPreferences.getString("id", ""));
        user.setEmail(sharedPreferences.getString("email",""));
        user.setName(sharedPreferences.getString("name",""));
        user.setSurname(sharedPreferences.getString("surname",""));
        user.setBirthday(sharedPreferences.getString("birthday",""));
        user.setCreated(sharedPreferences.getString("created",""));
        return user;
    }












//    public final String[] monthArray = new String[]{getString(R.string.month_1),
//            getString(R.string.month_2),
//            getString(R.string.month_3),
//            getString(R.string.month_4),
//            getString(R.string.month_5),
//            getString(R.string.month_6),
//            getString(R.string.month_7),
//            getString(R.string.month_8),
//            getString(R.string.month_9),
//            getString(R.string.month_10),
//            getString(R.string.month_11),
//            getString(R.string.month_12)};

    public static final String[] Months = new String[] { "Ամիս","Հունվար", "Փետրվար",
            "Մարտ", "Ապրիլ", "Մայիս", "Հունիս", "Հուլիս", "Օգոստոս", "Սեպտեմբեր",
            "Հոկտեմբեր", "Նոյեմբեր", "Դեկտեմբեր" };
}
