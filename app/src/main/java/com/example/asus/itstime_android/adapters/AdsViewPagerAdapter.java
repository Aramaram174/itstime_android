package com.example.asus.itstime_android.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.asus.itstime_android.R;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

public class AdsViewPagerAdapter extends PagerAdapter {
    Context mContext;
    LayoutInflater mLayoutInflater;
    private String[] mResources = {
            "http://qige87.com/data/out/97/wp-image-142768649.png",
            "http://www.idea-space.eu/uploads/idea_logos/256x256/76.png",
            "http://i1.wp.com/www.mad4detail.com/wp-content/uploads/2014/10/mad4detail-new-car-detail.png?resize=256%2C256",
            "http://www.travel-australia-online.com/images/mannum-sa-21553764.jpg",
    };

    public AdsViewPagerAdapter(Context context) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mResources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.ads_view_pager_item, container, false);
        ImageView imageView = itemView.findViewById(R.id.image_ads_iv);

        Transformation transformation = new RoundedTransformationBuilder()
                .borderWidthDp(0)
                .cornerRadiusDp(12)
                .build();

        Picasso.get().load(mResources[position]).fit().transform(transformation).into(imageView);
//        try {
//            Bitmap b =  Picasso.get().load(R.mipmap.ic_ads_defoult).fit().transform(transformation).get();  //TODO nkar chexac vaxt defoult nkar
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}