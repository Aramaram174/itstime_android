package com.example.asus.itstime_android.activities;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.util.Linkify;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.itstime_android.R;

public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener{
    private EditText forgot_email_et;
    private Button forgot_send_btn;
    private TextView forgot_re_send_link_tv;
    private ProgressBar forgot_pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_title_layout);
        TextView textView = findViewById(R.id.action_bar_custom_title);
        textView.setText(getResources().getString(R.string.forgot_title));

        forgot_email_et = findViewById(R.id.forgot_email_et);
        forgot_send_btn = findViewById(R.id.forgot_send_btn);
        forgot_send_btn.setOnClickListener(this);
        forgot_re_send_link_tv = findViewById(R.id.forgot_re_send_link_tv);
        forgot_re_send_link_tv.setPaintFlags(forgot_re_send_link_tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Linkify.addLinks(forgot_re_send_link_tv, Linkify.ALL);
        forgot_re_send_link_tv.setOnClickListener(this);
        forgot_pb = findViewById(R.id.forgot_pb);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.forgot_send_btn:
                sendPin();
                break;
            case R.id.forgot_re_send_link_tv:
                break;
        }
    }

    private void sendPin(){
        String email = forgot_email_et.getText().toString().trim();
        if (email.length() == 0) {
            Toast.makeText(this, getResources().getString(R.string.forgot_enter_email), Toast.LENGTH_SHORT).show();
            return;
        }else if (!isValidEmail(email)) {
            Toast.makeText(this, getResources().getString(R.string.signup_wrong_email), Toast.LENGTH_SHORT).show();
            return;
        }
        forgot_pb.setVisibility(View.VISIBLE);
        startActivity(new Intent(ForgotPasswordActivity.this, LoginActivity.class));
        finish();
    }

    private boolean isValidEmail(String email) {
        return email.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    }
}
