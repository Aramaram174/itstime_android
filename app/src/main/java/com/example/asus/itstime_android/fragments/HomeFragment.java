package com.example.asus.itstime_android.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.asus.itstime_android.R;
import com.example.asus.itstime_android.adapters.AdsViewPagerAdapter;

import me.relex.circleindicator.CircleIndicator;

public class HomeFragment extends Fragment {
    private View view;
    private ViewPager view_pager_ads;
    private AdsViewPagerAdapter adsViewPagerAdapter;
    private Handler handler;
    private final int delay = 2000;
    private int page = 0;

    Runnable runnable = new Runnable() {
        public void run() {
            if( page >= adsViewPagerAdapter.getCount()){
                page = 0;
            }else{
                page++;
            }
            view_pager_ads.setCurrentItem(page, true);
            handler.postDelayed(this, delay);
        }
    };


    public HomeFragment() {
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        handler = new Handler();
        view_pager_ads = view.findViewById(R.id.view_pager_ads);
        adsViewPagerAdapter = new AdsViewPagerAdapter(getContext());
        view_pager_ads.setAdapter(adsViewPagerAdapter);
        CircleIndicator indicator = view.findViewById(R.id.indicator);
        indicator.setViewPager(view_pager_ads);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        handler.postDelayed(runnable, delay);
    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
