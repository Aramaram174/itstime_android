package com.example.asus.itstime_android.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.util.Linkify;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.itstime_android.App;
import com.example.asus.itstime_android.R;
import com.example.asus.itstime_android.interfaces.ServicesTutorial;
import com.example.asus.itstime_android.models.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private Activity activity = this;
    private EditText login_email_et;
    private EditText login_password_et;
    private TextView login_forgot_password_link_tv;
    private CheckBox login_remember_cb;
    private Button login_btn;
    private TextView login_signup_link_tv;
    private ProgressBar login_pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_title_layout);
        TextView textView = findViewById(R.id.action_bar_custom_title);
        textView.setText(getResources().getString(R.string.login_title));

        login_email_et = findViewById(R.id.login_email_et);
        login_password_et = findViewById(R.id.login_password_et);
        login_forgot_password_link_tv = findViewById(R.id.login_forgot_password_link_tv);
        login_forgot_password_link_tv.setPaintFlags(login_forgot_password_link_tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Linkify.addLinks(login_forgot_password_link_tv, Linkify.ALL);
        login_forgot_password_link_tv.setOnClickListener(this);
        login_remember_cb = findViewById(R.id.login_remember_cb);
        login_btn = findViewById(R.id.login_btn);
        login_btn.setOnClickListener(this);
        login_signup_link_tv = findViewById(R.id.login_signup_link_tv);
        login_signup_link_tv.setPaintFlags(login_signup_link_tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Linkify.addLinks(login_signup_link_tv, Linkify.ALL);
        login_signup_link_tv.setOnClickListener(this);
        login_pb = findViewById(R.id.login_pb);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_forgot_password_link_tv:
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
                break;
            case R.id.login_btn:
                login();
                break;
            case R.id.login_signup_link_tv:
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
                break;
        }
    }

    private void login(){
        String email = login_email_et.getText().toString().trim();
        String password = login_password_et.getText().toString().trim();
        if (email.length() == 0) {
            Toast.makeText(this, getResources().getString(R.string.login_enter_email), Toast.LENGTH_SHORT).show();
            return;
        }else if (!isValidEmail(email)) {
            Toast.makeText(this, getResources().getString(R.string.login_wrong_email), Toast.LENGTH_SHORT).show();
            return;
        }
        if (password.length() == 0) {
            Toast.makeText(this, getResources().getString(R.string.login_enter_password), Toast.LENGTH_SHORT).show();
            return;
        }else if (password.length() < 8){
            Toast.makeText(this, getResources().getString(R.string.login_min_password), Toast.LENGTH_SHORT).show();
            return;
        }
        login_pb.setVisibility(View.VISIBLE);
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
//        login(user);
        loginTest();
    }

    private boolean isValidEmail(String email) {
        return email.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    }

    private void login(User user){
        ServicesTutorial servicesTutorial = ServicesTutorial.retrofit.create(ServicesTutorial.class);
        Call<User> call = servicesTutorial.login(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                login_pb.setVisibility(View.GONE);
                if (response.body() != null) {
                    App.i().saveUserData(response.body());
                    if (login_remember_cb.isChecked()) {
                        App.i().saveRemember(true);
                    }else {
                        App.i().saveRemember(false);
                    }
                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    finish();
                } else {
                    Toast.makeText(activity, getResources().getString(R.string.login_wrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                login_pb.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.not_found_internet_connection), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loginTest(){
        if (login_remember_cb.isChecked()){
            App.i().saveRemember(true);
        }
        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
        finish();
    }


}
