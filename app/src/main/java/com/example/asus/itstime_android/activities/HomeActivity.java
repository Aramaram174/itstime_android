package com.example.asus.itstime_android.activities;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.example.asus.itstime_android.R;
import com.example.asus.itstime_android.adapters.FragmentViewPagerAdapter;
import com.luseen.luseenbottomnavigation.BottomNavigation.BottomNavigationView;
import com.luseen.luseenbottomnavigation.BottomNavigation.OnBottomNavigationItemClickListener;

public class HomeActivity extends BaseActivity implements ViewPager.OnPageChangeListener{
    private BottomNavigationView bottom_navigation_view;
    private FragmentViewPagerAdapter fragmentViewPagerAdapter;
    private ViewPager view_pager_fragments;

    private int[] colors = {R.color.colorYellow, R.color.colorGreen, R.color.colorRed};
    private int[] images = {R.drawable.ic_home, R.mipmap.ic_calendar, R.mipmap.ic_note};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_title_layout);
        TextView textView = findViewById(R.id.action_bar_custom_title);
        textView.setTextSize(25);
        textView.setText(getResources().getString(R.string.app_name));

        view_pager_fragments = findViewById(R.id.view_pager_fragments);
        bottom_navigation_view = findViewById(R.id.bottom_navigation_view);
        fragmentViewPagerAdapter = new FragmentViewPagerAdapter(getSupportFragmentManager());
        view_pager_fragments.setAdapter(fragmentViewPagerAdapter);
        bottom_navigation_view.setUpWithViewPager(view_pager_fragments, colors, images);
        bottom_navigation_view.isColoredBackground(false);
        bottom_navigation_view.setItemActiveColorWithoutColoredBackground(R.color.colorAccent);
        bottom_navigation_view.willNotRecreate(true);
        bottom_navigation_view.disableViewPagerSlide();
        bottom_navigation_view.setTextActiveSize(30);
        bottom_navigation_view.setTextInactiveSize(25);
        bottom_navigation_view.setOnBottomNavigationItemClickListener(new OnBottomNavigationItemClickListener() {
            @Override
            public void onNavigationItemClick(int index) {
            }
        });

        final View touchView = findViewById(R.id.view_pager_fragments);
        touchView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Fragment fragment = fragmentViewPagerAdapter.getFragment(position);
        if (fragment != null) {
            fragment.onResume();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
