package com.example.asus.itstime_android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.asus.itstime_android.R;

public class BirthdayActivity extends AppCompatActivity {
    private FloatingActionButton add_birthday_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_birthday);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_title_layout);
        TextView textView = findViewById(R.id.action_bar_custom_title);
        textView.setText(getResources().getString(R.string.birthday_activity_title));

        add_birthday_btn = findViewById(R.id.add_birthday_btn);
        add_birthday_btn.setOnClickListener(v -> {
            startActivity(new Intent(BirthdayActivity.this, EditBirthdayActivity.class));
        });
    }
}
